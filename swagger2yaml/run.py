"""
Created on 2019-07-05
@author: FUQIANG523
"""

import os
import sys
import json
import shutil
import requests
import pyaml

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)


def get_url(url, param):
    """
    获取swagger url
    """
    url_param = '{%s}' % param
    if url_param in url:
        url = str(url).replace(url_param, '$' + param)
    if '?' in url:
        url += '&'
    else:
        url += '?'
    return url + param + '=$' + param


def get_definitions(definitions_dict, definitions_name):
    """
    获取response的definitions
    """
    ret_dict = {}
    definitions_name = definitions_name.split('definitions/')[-1]
    req_param_list = definitions_dict[definitions_name]  # dict
    if 'properties' in req_param_list.keys():
        for item in req_param_list['properties'].keys():
            if '$ref' in req_param_list['properties'][item].keys():
                get_definitions(definitions_dict,
                                req_param_list['properties'][item]['$ref'])
            else:
                if 'description' in req_param_list['properties'][item].keys():
                    ret_dict[item] = '$' + item + '# ' \
                                     + req_param_list['properties'][item]['description']
                elif 'type' in req_param_list['properties'][item].keys():
                    ret_dict[item] = '$' + item + '# ' + \
                        req_param_list['properties'][item]['type']
                else:
                    ret_dict[item] = '$' + item
    return ret_dict


def response_get(url):
    """
    获取response
    """
    try:
        response = requests.get(url)
        return json.loads(response.text.replace(',]', ']'))
    except Exception as err:
        return err


def project_add(project_name):
    """
    调用hrun指令，新建项目或项目框架
    """
    project_path = os.path.join(BASE_DIR, project_name)
    if os.path.exists(project_path):
        shutil.rmtree(project_path)
    res_status = os.system('hrun --startproject %s' % project_name)
    if res_status == 0:
        return True
    else:
        return False


def response_parse(in_dict):
    """
    解析response
    """
    apis_all = {}
    api_url_lists = []
    if isinstance(in_dict, dict):
        apis_data = in_dict['paths']
        for api_url in apis_data.keys():
            api_url_lists.append(api_url)
        for api_url in api_url_lists:
            apis_all[api_url] = {}
            for req_method in apis_data[api_url].keys():
                apis_all[api_url][req_method] = {}
                api_dict = {
                    'a_variables': {
                        'a_api_name': None,
                        'b_api_path': None,
                        'c_uri': None,
                    },
                    'request': {
                        'a_url': '$uri',
                        'b_method': None,
                        'c_headers': {
                            'Cookie': '$cookie',
                        },
                    }
                }
                testcases_list = [  # testcases List
                    {
                        'a_config': {
                            'name': None
                        }
                    },
                    {
                        'test': {
                            'a_name': None,
                            'b_api': None,
                            # 'c_variables': {
                            #     'test': None
                            #     },
                            'd_validate': ['eq: value_validate']
                        }
                    }
                ]
                testsuites_dict = {  # testsuites Dict
                    'config': {
                        'a_name': None,
                        'b_base_url': '${base_url()}'
                    },
                    'testcases': {
                        # 'api_name': {
                        #     'testcase': 'testcase path'
                        #     }
                    }
                }

                tags_name = apis_data[api_url][req_method]['tags'][0]
                # api_name = apis_data[api_url][req_method]['summary']
                api_name = apis_data[api_url][req_method]['summary']
                # operate_name = apis_data[api_url][req_method]['operationId']
                operate_name = api_url[1:].replace(
                    '/', '_').replace('{', '').replace('}', '')
                api_path = 'api/' + str(tags_name) + \
                    '/' + str(operate_name) + '.yml'
                testcase_path = 'testcases/' + str(operate_name) + '_case.yml'

                testsuites_dict['config']['a_name'] = api_name
                testsuites_dict['testcases'] = {
                    api_name: {
                        'testcase': testcase_path,
                    }
                }

                testcases_list[0]['a_config']['name'] = api_name
                testcases_list[1]['test']['a_name'] = api_name
                testcases_list[1]['test']['b_api'] = api_path

                api_dict['a_variables']['a_api_name'] = api_name
                api_dict['a_variables']['b_api_path'] = api_path
                api_dict['request']['b_method'] = req_method
                new_api_url = api_url
                if 'parameters' not in apis_data[api_url][req_method].keys():
                    pass
                else:
                    new_api_url = api_url
                    for sub_item in apis_data[api_url][req_method]['parameters']:
                        if sub_item['in'] in ['path', 'query']:
                            new_api_url = get_url(
                                new_api_url, sub_item['name'])
                        if sub_item['in'] == 'formData':
                            api_dict['request']['data'] = '$multipart_encoder'
                        if sub_item['in'] == 'body':
                            if 'schema' in sub_item.keys():
                                if '$ref' in sub_item['schema'].keys():
                                    ref = sub_item['schema']['$ref']
                                    req_body = get_definitions(
                                        in_dict['definitions'], ref)
                                    api_dict['request']['json'] = req_body
                            else:
                                api_dict['request']['json'] = {
                                    sub_item['name']: '$' + sub_item['name']
                                }
                api_dict['a_variables']['c_uri'] = new_api_url
                apis_all[api_url][req_method] = {
                    'tags': tags_name,
                    'opertaor_name': operate_name,
                    'api': api_dict,
                    'testcases': testcases_list,
                    'testsuites': testsuites_dict,
                }
    return apis_all


def write_yaml(project_name, api_data):
    """
    写yaml文件
    """
    project_path = os.path.join(BASE_DIR, project_name)

    # api_path = os.path.join(project_path, 'api')
    # testcases_path = os.path.join(project_path, 'testcases')
    # testsuites_path = os.path.join(project_path, 'testsuites')

    api_tags_name = api_data['tags']
    api_operator = api_data['opertaor_name']
    # api_data = api_data['api']
    # testcases_data = api_data['testcases']
    # testsuites_data = api_data['testsuites']

    api_yaml = pyaml.dump(api_data['api'])
    testcases_yaml = pyaml.dump(api_data['testcases'])
    testsuites_yaml = pyaml.dump(api_data['testsuites'])

    api_dir = project_path + os.sep + 'api' + os.sep + api_tags_name

    if not os.path.exists(api_dir):
        os.makedirs(api_dir)

    with open(api_dir + os.sep + api_operator + '.yml', 'w+', encoding='utf-8') as f_api:
        f_api.write(api_yaml.replace('  ', '    ').replace("'", '').replace('# ', '    # ').replace('a_api_name', 'api_name').replace('b_api_path', 'api_path')
                    .replace('c_uri', 'uri').replace('a_url', 'url').replace('b_method', 'method')
                    .replace('c_headers', 'headers').replace('a_variables', 'variables')
                    .replace('request:', '\nrequest:')
                    )

    with open(project_path + os.sep + 'testcases' + os.sep + api_operator + '_case.yml',
              'w+', encoding='utf-8') as f_cases:
        f_cases.write(testcases_yaml.replace('a_config', 'config').replace('a_name', 'name')
                      .replace("- 'eq: value_validate'", "  - eq: ['status_code', 200]")
                      .replace('b_api', 'api').replace('d_validate', 'validate')
                      .replace('- test:', '\n- test:')
                      )

    with open(project_path + os.sep + 'testsuites' + os.sep + api_operator + '_suite.yml', 'w+',
              encoding='utf-8') as f_suites:
        f_suites.write(
            testsuites_yaml.replace('  ', '    ').replace('a_name', 'name').replace('b_base_url', 'base_url').replace(
                'testcases:', '\ntestcases:'))


def start(project_name, url):
    """
    开始执行方法
    """
    in_dict = response_get(url)
    apis_all = response_parse(in_dict)
    for api_url in apis_all.keys():
        for api_method in apis_all[api_url].keys():
            write_yaml(project_name, apis_all[api_url][api_method])


if __name__ == '__main__':
    print('Start...')
    #######################################################
    SWAGGER_URL = 'http://vtest.top/swagger/v2/api-docs'
    PROJECT_NAME = 'oms_platform_interfaces'
    #######################################################
    project_add(PROJECT_NAME)
    start(PROJECT_NAME, SWAGGER_URL)
    print('Done...')
