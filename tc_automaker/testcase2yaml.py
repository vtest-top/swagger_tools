import os
import pyaml


class Tc2Yaml:
    def __init__(self, project_path, api_sutie):
        self.project_path = project_path
        self.name = api_sutie['name']
        self.path = api_sutie['path'].replace('{', '$').replace('}', '')
        self.api_file = str(api_sutie['path'])[1:].replace('/', '_').replace(
            '{', '').replace('}', '')
        self.method = api_sutie['method']
        self.operation = api_sutie['operation']
        self.tags = api_sutie['tags']
        self.params = api_sutie['params']  # list
        self.testcase = api_sutie['testcases']

    def api_yaml(self):
        if os.path.exists(self.project_path + '/api/' + self.tags):
            pass
        else:
            os.mkdir(self.project_path + '/api/' + self.tags)
        api_dict = {
            'a_variables': {
                'aapi_name': self.name,
                'api_file':
                'api/' + str(self.tags) + '/' + self.api_file + '.yml',
                'uri': self.path
            },
            'request': {
                'a_url': '$uri',
                'b_method': self.method,
                'c_headers': {
                    'Cookie': '$cookie'
                }
            }
        }

        for index, param in enumerate(self.params):
            if param['in'] == 'header':
                api_dict['request']['c_headers'][
                    param['name']] = '$' + param['name'] + 'VTest' + str(
                        param['description'])
                del self.params[index]
            elif param['in'] == 'path' and ('$' +
                                            param['name']) not in self.path:
                self.path = self.path + '?' + param['name'] + '=$' + param[
                    'name']
                del self.params[index]

        if self.method.lower() != 'get':
            api_dict['z_json'] = {}
            for param in self.params:
                if '$' + param['name'] not in self.path:
                    api_dict['z_json'][param['name']] = '$' + param[\
                        'name'] + 'VTest' + str(param['description'])

        with open(self.project_path + '/api/' + self.tags + '/' +
                  self.api_file + '.yml',
                  'w+',
                  encoding='utf-8') as f:
            f.write(
                pyaml.dump(api_dict).replace('a_variables', 'variables').\
                    replace('a_url', 'url').replace('b_method', 'method').\
                    replace('c_headers','headers').replace('request', '\nrequest').\
                    replace('aapi_name', 'api_name').replace('z_json','\njson').replace('VTest', '  # '))

    def testsuite_yaml(self):
        if os.path.exists(self.project_path + '/testsuites/' + self.tags):
            pass
        else:
            os.mkdir(self.project_path + '/testsuites/' + self.tags)
        testsuite_dict = {
            'config': {
                'a_name': str(self.name) + '_接口套件',
                'base_url': '${base_url()}'
            },
            'testcases': {
                self.name + '_接口': {
                    'testcase':
                    'testcases/' + self.tags + '/' + self.api_file +
                    '_suite.yml'
                }
            }
        }
        with open(self.project_path + '/testsuites/' + self.tags + '/' +
                  self.api_file + '_case.yml',
                  'w+',
                  encoding='utf-8') as f:
            f.write(
                pyaml.dump(testsuite_dict).replace('testcases:',
                                                   '\ntestcases:').replace(
                                                       'a_name', 'name'))

    def testcase_yaml(self):
        if os.path.exists(self.project_path + '/testcases/' + self.tags):
            pass
        else:
            os.mkdir(self.project_path + '/testcases/' + self.tags)
        testcase_list = [{
            'config': {
                'name': self.name + '_接口用例',
            }
        }]

        for index, i in enumerate(self.testcase):
            tc_fields_list = i[0]['request']
            tc_dict = {}
            for tc_value in tc_fields_list:
                tc_dict[tc_value['fieldName']] = tc_value['fieldValue']
            testcase_sub = {
                'test': {
                    'aa_name': i[1],
                    'api': 'api/' + self.tags + '/' + self.api_file + '.yml',
                    'variables': tc_dict,
                    # 'validate': [{
                    #     'eq': ['content.code', 200]
                    # }]
                }
            }
            testcase_list.append(testcase_sub)
        with open(self.project_path + '/testcases/' + self.tags + '/' +
                  self.api_file + '_case.yml',
                  'w+',
                  encoding='utf-8') as f:
            f.write(
                pyaml.dump(testcase_list).replace('aa_name', 'name').replace(
                    '- test:', '\n- test:'))

    def run(self):
        self.api_yaml()
        self.testsuite_yaml()
        self.testcase_yaml()
        return True
