"""解析json"""

import os
import shutil


class ParseJson:
    """解析swagger json"""
    def __init__(self, name, content, project_dir):
        """初始化"""
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.project_path = project_dir
        self._name = name
        self._version = content['swagger']
        self._info = content['info']
        self._host = content['host']
        self._tags = content['tags']
        self._paths = content['paths']
        self._definitions = content['definitions']
        self.format = []

    def gen_hrun(self):
        """新建hrun项目"""
        if os.path.exists(self.project_path):
            shutil.rmtree(self.project_path)
        res_status = os.system('hrun --startproject {}'.format(self._name))
        if res_status == 0:
            print('新建[{}]成功。'.format(self._name))
            return True
        else:
            print(
                '新建[{}]失败，请联系傅强(FUQIANG523)(FUQIANG523@pingan.com.cn)。'.format(self._name))
            return False

    def tags(self):
        """模块解析并新建文件夹"""
        for tag in self._tags:
            api_dir = self.project_path + os.sep + 'api' + os.sep + tag['name']
            if not os.path.exists(api_dir):
                os.makedirs(api_dir)

    # @staticmethod
    def parse_param(self, api_params: list):
        all_params = []
        for api_param in api_params:
            param_name = api_param['name']
            param_in = api_param['in']
            is_required = api_param['required']
            param_format = None
            param_desc = None
            param_type = None
            default_value = None
            param_schema = None
            api_param_keys = api_param.keys()
            if 'type' in api_param_keys:
                param_type = api_param['type']
            if 'format' in api_param_keys:
                param_format = api_param['format']
            if 'description' in api_param_keys:
                param_desc = api_param['description']
            if 'default' in api_param_keys:
                default_value = api_param['default']
            if 'schema' in api_param_keys:
                param_schema = api_param['schema']
            self.format.append(param_format)
            param_dict = {
                'name': param_name,
                'field': param_name,
                'in': param_in,
                'is_required': is_required,
                'format': param_format,
                'description': param_desc,
                'type': param_type,
                'default': default_value,
                'schema': param_schema,
                'attrs': {}
            }
            if param_format == 'string':
                param_dict['attrs'] = {'type': '1'}
            elif param_format in ['int32', 'int64']:
                param_dict['attrs'] = {'type': '2'}
            elif param_format == 'date-time':
                param_dict['attrs'] = {'type': '4'}
            elif param_format == 'double':
                param_dict['attrs'] = {'type': '3'}
            else:
                param_dict['attrs'] = {'type': '1'}
            all_params.append(param_dict)
        return all_params

    def paths(self):
        """路径解析"""
        apis_base = []
        for path in self._paths.keys():
            for method in self._paths[path].keys():
                api_tags = self._paths[path][method]['tags'][0]
                api_summary = self._paths[path][method]['summary']
                api_operation_id = self._paths[path][method]['operationId']
                try:
                    api_parameters = self.parse_param(
                        self._paths[path][method]['parameters'])
                except Exception as err:
                    api_parameters = {}
                if len(api_parameters) == 1:
                    it = api_parameters[0]
                    if it['schema']:
                        if '$ref' in it['schema'].keys():
                            definition_name = it['schema']['$ref'][14:]
                            api_parameters = self.definitions(definition_name)
                        elif 'items' in it['schema'].keys():
                            it_x = it['schema']['items'].get('$ref', None)
                            if it_x:
                                definition_name = it['schema']['items']['$ref'][14:]
                                api_parameters = self.definitions(definition_name)

                api_dict = {
                    'name': api_summary,
                    'path': path,
                    'method': method,
                    'operation': api_operation_id,
                    'tags': api_tags,
                    'params': api_parameters
                }

                apis_base.append(api_dict)
        return apis_base

    @staticmethod
    def tc_param(pm_list: list):
        ret_dict = {}
        for i in pm_list:
            ret_dict[i['name']] = {
                'type': i['type'] if i['type'] else 'string',
                'value': '',
                'range': None,
                'iscompulsory': i['is_required']
            }
        return ret_dict

    def api_module(self):
        api_base = self.paths()
        # for item in api_base:
        #     req_data = item['params']
        #     tc = self.tc_param(req_data)
        #     item['tc'] = tc
        return api_base

    @staticmethod
    def gen_length(i_dict):
        min_length = 0
        max_length = 8
        if isinstance(i_dict, dict):
            if 'minLength' in i_dict.keys():
                min_length = i_dict['minLength']
            if 'maxLength' in i_dict.keys():
                max_length = i_dict['maxLength']
            if 'minimum' in i_dict.keys():
                min_length = i_dict['minimum']
            if 'maximum' in i_dict.keys():
                max_length = i_dict['maximum']

        return min_length, max_length

    def definitions(self, path=None):
        ret_list = []
        definition_dict = self._definitions
        definition = definition_dict[path]
        for item in definition['properties'].keys():
            item_dict = definition['properties'][item]
            min_length, max_length = self.gen_length(item_dict)
            format = item_dict['format'] if 'format' in item_dict.keys(
            ) else 'string'
            self.format.append(format)
            param_dict = {
                'name': item,
                'field': item,
                'in': 'body',
                'is_required': True, 
                'type': item_dict.get('type', 'string'),
                'format': format,
                'description': item_dict['description'] if 'description' in item_dict.keys() else item,
                'min_length': min_length,
                'max_length': max_length,
                'attrs': {}
            }
            if format == 'string':
                param_dict['attrs'] = {
                    'type': '1',
                    'min_length': str(min_length),
                    'max_length': str(max_length)
                }
            elif format in ['int32', 'int64']:
                param_dict['attrs'] = {
                    'type': '2',
                    'start': str(int(min_length)),
                    'end': str(int(max_length))
                }
            elif format == 'date-time':
                param_dict['attrs'] = {'type': '4'}
            elif format == 'double':
                param_dict['attrs'] = {
                    'type': '3',
                    'start': str(min_length),
                    'end': str(max_length),
                    'precision': '2'
                }
            else:
                param_dict['attrs'] = {'type': '1'}
            ret_list.append(param_dict)
        return ret_list
