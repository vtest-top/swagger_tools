import os
import json
import requests

from swagger_parse import ParseJson
from testcase2yaml import Tc2Yaml
from auto_tc_gen import GenTeseCases


def get_swagger_api(url=None):
    """
    从swagger地址中获取到接口json数据包
    :return:
    """

    response = requests.get(url)
    api_data = json.loads(response.text.replace(',]', ']'))

    # import json
    # with open('api_desc.json', 'r+', encoding='utf-8') as fls:
    #     api_data = json.loads(fls.read())

    api_dict = {
        'swagger': api_data['swagger'],
        'info': api_data['info'],
        'host': api_data['host'],
        'tags': api_data['tags'],
        'paths': api_data['paths'],
        'definitions': api_data['definitions']
    }

    return api_dict


def start(sys_name, project_dir, url):
    api_content = get_swagger_api(url)
    api_parse_obj = ParseJson(sys_name, api_content, project_dir)
    hrun_result = api_parse_obj.gen_hrun()
    # hrun_result = True
    if hrun_result:
        pass
    else:
        exit()
    res = api_parse_obj.api_module()
    # import json
    # with open('t.json', 'w+', encoding='utf-8') as f:
    #     f.write(json.dumps(res, ensure_ascii=False))
    for item in res:
        gen_tc = GenTeseCases(item)
        tc = gen_tc.gen_testcases()
        item['testcases'] = tc
    return res


def project_path(project_name):
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(BASE_DIR, project_name)


if __name__ == '__main__':
    url = 'http://vtest.top/swagger/v2/api-docs'
    print('Start...')
    project_name = 'test_demo1'
    project_dir = project_path(project_name)
    rets = start(project_name, project_dir, url)
    for ret in rets:
        tc2yaml = Tc2Yaml(project_dir, ret)
        tc2yaml.run()
    print('Done...')
