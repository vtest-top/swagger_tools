# 解析过程

    swagger     # 版本号
    info        # 基本信息：系统名称等
    host        # host
    basePath    # basePath
    tags:       # 模块名, type-list, list-name, list-description
    paths       # 接口路径, type-dict
        keys: 请求方式, type-dict
            operationId: ID, 唯一, 用于去yaml文件名
            parameters: 参数, type-list
                name: 参数名
                in: 所在位置, [path, formData, query, body, header]
                description: 描述
                required: 是否必填, [false, true, type-list]
                type: 类型, [integer, string, boolean, array, file]
                format: 格式, [int64, int32, date-time]
                default: 默认值
                schema: 定义对象
            responses: 响应
    definitions # 定义对象,

# automaker

    'username': {
            'type': 'string',  # 字符串 [string, object, number, int, date, array]
            'value': '',
            'range': None,  # 范围
            'iscompulsory': True    #是否为必填
    }
